Image for NodeJS service with nodemon entry point

# About
- this image included:
    - alpine
    - node
    - npm
    - nodemon
- on build image automaticaly update npm dependency
- workdir - `/app`
- entry point of this image is `node <command>`

# Usage


# Example

run with Dockerfile:
```
FROM thedronzik/node-service:latest

MAINTAINER ACME

EXPOSE 10001

# in prod mode app is partial of image
COPY . /app

RUN npm install

# in dev mode app and mode_modules overload
VOLUME ["/app", "/app/node_modules"]

CMD ["application.js --wp=10001 --mp=10002 --mode=redis --storage=redis --channel=EVENTS"]
```

run with compose:
```
version: '3'

volumes:
  bundles:

services:

  node-message:
    image: thedronzik/node-service
    links:
        - redis
        - memcached
    expose:
      - "10001"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - .:/app
    command: ["application.js"]
    depends_on:
      - redis
      - memcached
```